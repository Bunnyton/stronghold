### Установка
Чтобы добавить apt репозиторий необходимо выполнить следующие действия:

1. Устанавливаем утилиту для получения web страниц.
```
sudo apt install curl
```
2. Получаем GPG ключ репозитория и добавляем его в доверенные:
```
curl -s --compressed "https://bunnyton.gitlab.io/stronghold/KEY.gpg" | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/hashpass.gpg
```
3. Добавляем репозиторий в список apt репозиториев:
```
sudo su
```
```
echo "deb https://bunnyton.gitlab.io/stronghold ./" > /etc/apt/sources.list.d/it_bastion.list
```
```
exit
```
4. Обновляем список репозиториев:
```
sudo apt update
```

После этого можем устанавливать ПО для лабораторных работ:
```
sudo apt install hashpass
```

### Проблемы
#### Возможная проблема 1
Если вдруг вы получаете ошибку:
```
the pre-installation script returned error code 1
```
Это означает, вероятнее всего, что Ваш хост не может найти и поставить пакет docker.io, который является необходимой составляющей для ПО. Проверить его наличие можно следующим способом:
```
dpkg -s docker.io
```
Проверить, доступен ли пакет для скачивания с текущих репозиториев:
```
sudo apt search docker.io
```
Если пакет не доступен для скачивания, то Вам необходимо попытаться добавить репозиторий для docker. Для этого необходимо изучить следующую инструкцию для [amd64 (Intel)](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru) или для [arm64 (M1)](https://www.docker.com/blog/getting-started-with-docker-for-arm-on-linux/)

Проверить работоспособность можно при помощи команды:
```
sudo docker images
```
Если все хорошо, то выведет:
```
REPOSITORY                   TAG                                        IMAGE ID       CREATED         SIZE
```

Возможно, у Вас при этом не установится пакет docker.io. В таком случае, необходимо заменить 
```
sudo apt install hashpass
```
на 
```
sudo apt install hashpass-alt
```

#### Возможная проблема 2
Если вдруг помимо:
```
the pre-installation script returned error code 1
```
Вы получаете следующее:
```
no space left on device
```
То это означает, что Вам не хватает памяти на устройстве.
Дело в том, что docker по умолчанию использует каталог /var/lib/docker для хранения информации. Соответственно, занимает место на /. Если под него было недостаточно выделено памяти, то можно перенести каталог хранения docker в любую удобную папку (не забудьте поменять user на имя Вашего пользователя):
```
sudo su
```
```
service docker stop
```
```
cd /home/user/
```
```
mkdir Install
```
```
mv /var/lib/docker ./Install/
```
```
echo "{\"data-root\": \"/home/user/Install/docker\"}" > /etc/docker/daemon.json
```
```
service docker start
```
```
exit
```
Здесь вместо user необходимо подставить ваше имя пользователя, посмотреть его можно при помощи команды (если показывает root, выйдите из режима суперпользователя (exit) и повторите команду). 
```
whoami
```
Для хранения docker используется директория /home/user/Install, вы можете ее заменить на любую удобную для Вас
